<?php
include_once("../cabecalho.php");
?>
    <div id="map"></div>
    <script>
        function initMap() {
            var myLatLng = {lat: -25.363, lng: 131.044};

            // Create a map object and specify the DOM element for display.
            var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                scrollwheel: false,
                zoom: 4
            });

            // Create a marker and set its position.
            var marker = new google.maps.Marker({
                map: map,
                position: myLatLng,
                title: 'Hello World!'
            });
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcba0lHgbv_YPQCP7AEAu2Z0_zEyWC304&callback=initMap"
            async defer></script>
    <section class="container">

        <form class="form-horizontal" method="post" class="form" action="cadastro.php" >
            <div class="form-group">
                <label for="descricao">Descrição:</label>
                <textarea class="form-control" id="descricao" rows="5"></textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Foto:</label>
                <input type="file" id="exampleInputFile">
            </div>
            <div class="form-group">
                <label for="data">Data ocorrido </label>
                <input type="date" class="form-control" id="data">
            </div>
            <div class="form-group">
                <label for="hora">Hora ocorrido</label>
                <input type="time" class="form-control" id="hora">
            </div>
            <input type="hidden" id="localização" value="">
            <button type="submit" class="btn btn-default">Submit</button>
        </form>

    </section>

<?php include_once("../rodape.php"); ?>