<?php
$title_name="Socorre Me/Cadastro";
$folha_default="../css/style.css";
$folha_estilo="#";
$home="../index.php";
$sobre="../sobre.php";
$logar="loginusuario.php";
$cadastrar="#";
include_once("../cabecalho.php");
?>

<section class="container">
    <form class="form-horizontal" method="post" class="form" action="cadastro.php" >
        <div class="form-group">
            <label for="usuario">Usuário:</label>
            <input type="text" class="form-control" id="usuario" placeholder="Digite o usuário">
        </div>
        <div class="form-group">
            <label for="email">E-mail:</label>
            <input type="email" class="form-control" id="email" placeholder="Digite o e-mail">
        </div>
        <div class="form-group">
            <label for="pwd">Senha: </label>
            <input type="password" class="form-control" id="pwd" placeholder="Digite a senha">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</section>

<?php include_once("../rodape.php"); ?>
