<?php
$title_name="Socorre Me/login";
$folha_default="../css/style.css";
$folha_estilo="#";
$home="../index.php";
$sobre="../sobre.php";
$logar="#";
$cadastrar="cadastrousuario.php";
include_once("../cabecalho.php");
?>
<section class="container">
    <form class="form-horizontal" method="post" class="form" action="login.php" >
        <div class="form-group">
            <label for="usuario">Usuário:</label>
            <input type="text" class="form-control" id="usuario" placeholder="Digite o usuário">
        </div>
        <div class="form-group">
            <label for="pwd">Senha: </label>
            <input type="password" class="form-control" id="pwd" placeholder="Digite a senha">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</section>


<?php include_once("../rodape.php"); ?>
