<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<title><?php print $title_name ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="socorre-me, segurança, UFPA"
	<meta name="author" content="">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="<?php print $folha_default ?>">
	<link rel="stylesheet" type="text/css" href="<?php print $folha_estilo ?>" >
	<link rel="stylesheet" href="/maps/documentation/javascript/demos/demos.css">

</head>
<body>
<header class="container">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php print $home ?>">Socorre-me</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="<?php print $home ?>">Home</a></li>
				<li><a href="<?php print $sobre ?>">Sobre</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?php print $logar ?>"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
				<li><a href="<?php print $cadastrar ?>"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			</ul>
		</div>
	</nav>
</header>